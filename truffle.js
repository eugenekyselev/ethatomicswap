module.exports = {
  networks: {
    development: {
      host: "localhost",
      port: 8545,
      network_id: "*" // Match any network id
    },
      rinkeby: {
          host: "localhost", // Connect to geth on the specified
          port: 8545,
          from: "0x66bb8643A63D0D5E974cc81F4Bb8EDfD6abEC108", // default address to use for any transaction Truffle makes during migrations
          network_id: 4,
          gas: 4612388 // Gas limit used for deploys
      }
  }
};
